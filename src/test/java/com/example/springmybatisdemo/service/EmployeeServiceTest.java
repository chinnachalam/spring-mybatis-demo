package com.example.springmybatisdemo.service;

import com.example.springmybatisdemo.exception.APIException;
import com.example.springmybatisdemo.model.Employee;
import com.example.springmybatisdemo.repository.EmployeeMyBatisRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;

public class EmployeeServiceTest {

    EmployeeMyBatisRepository employeeMyBatisRepository = Mockito.mock(EmployeeMyBatisRepository.class);

    @InjectMocks
    EmployeeService employeeService = new EmployeeService(employeeMyBatisRepository);

    @Test
    public void test_getEmployee() {
        Employee employee = new Employee(100100l, "John", "test", "test@gmail.com");
        //Employee employee = Mockito.mock(Employee.class);
        Mockito.when(employeeMyBatisRepository.findById(100100l)).thenReturn(employee);

        Employee response = employeeService.getEmployee(100100l);

        Assertions.assertNotNull(response);

        // Mockito.verify(employeeMyBatisRepository.findById(100100l), Mockito.times(1));
        Assertions.assertEquals(100100l, response.getId());
        Assertions.assertEquals("John", response.getFirstName());
    }

    @Test
    public void test_getEmployee_with_exception() {

        Mockito.when(employeeMyBatisRepository.findById(100100l)).thenReturn(null);

        Assertions.assertThrows(APIException.class, () -> {
            employeeService.getEmployee(100100l);
        });

    }

}
