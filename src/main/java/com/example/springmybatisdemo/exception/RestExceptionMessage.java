package com.example.springmybatisdemo.exception;

import org.springframework.http.HttpStatus;

public class RestExceptionMessage {
    private final String message;
    private final HttpStatus status;

    public RestExceptionMessage(String message, HttpStatus status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public HttpStatus getStatus() {
        return status;
    }
}