package com.example.springmybatisdemo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(APIException.class)
    public final ResponseEntity<RestExceptionMessage> somethingWentWrong(APIException ex) {
        RestExceptionMessage restExceptionMessage = new RestExceptionMessage(ex.getMessage(), ex.getErrorCode().status);
        return new ResponseEntity<>(restExceptionMessage, restExceptionMessage.getStatus());
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<RestExceptionMessage> somethingWentWrong(Exception ex) {
        RestExceptionMessage restExceptionMessage = new RestExceptionMessage(ex.getMessage(), HttpStatus.NOT_FOUND);
        return new ResponseEntity<>(restExceptionMessage, restExceptionMessage.getStatus());
    }
}
