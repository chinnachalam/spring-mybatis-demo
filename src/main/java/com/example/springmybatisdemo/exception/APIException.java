package com.example.springmybatisdemo.exception;


import org.springframework.http.HttpStatus;

public class APIException extends RuntimeException {


    private final String message;
    private final ErrorCode errorCode;



    public APIException(ErrorCode code) {
        this.errorCode = code;
        this.message = code.message;
    }

    public APIException(ErrorCode errorCode, String message) {
        this.errorCode = errorCode;
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }




    public enum ErrorCode {
        NOT_FOUND("Data not found", HttpStatus.NOT_FOUND);

        public final String message;
        public final HttpStatus status;

        ErrorCode(String message, HttpStatus status) {
            this.message = message;
            this.status = status;
        }
    }
}