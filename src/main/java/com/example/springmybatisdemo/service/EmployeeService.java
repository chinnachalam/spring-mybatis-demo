package com.example.springmybatisdemo.service;

import com.example.springmybatisdemo.exception.APIException;
import com.example.springmybatisdemo.model.Employee;
import com.example.springmybatisdemo.repository.EmployeeMyBatisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    //private final String data;

    public EmployeeMyBatisRepository employeeMyBatisRepository;

    public EmployeeService(EmployeeMyBatisRepository employeeMyBatisRepository) {
        this.employeeMyBatisRepository = employeeMyBatisRepository;
    }

    public Employee getEmployee(Long empId) {
        System.out.println("EmployeeService called");
        Employee employee = employeeMyBatisRepository.findById(empId);
        if (employee == null) {
            throw new APIException(APIException.ErrorCode.NOT_FOUND, "Employee " + empId +" not found");
        } else {
            return employee;
        }
    }

    public List<Employee> getAllEmployees() {
        return employeeMyBatisRepository.findAll();
    }
}
