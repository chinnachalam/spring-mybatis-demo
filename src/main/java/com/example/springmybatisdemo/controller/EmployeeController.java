package com.example.springmybatisdemo.controller;

import com.example.springmybatisdemo.model.Employee;
import com.example.springmybatisdemo.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class EmployeeController {

    //@Autowired
    EmployeeService employeeService;

    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @GetMapping("/employee")
    public Employee getEmployee(@RequestParam Long employeeId) {
        System.out.println("EmployeeController called");
        return employeeService.getEmployee(employeeId);
    }

    @GetMapping("/all-employees")
    public List<Employee> getAllEmployees() {
        return employeeService.getAllEmployees();
    }
}
